package sda.jpa;

import sda.jpa.model.Klient;
import sda.jpa.model.OcenaKsiazki;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example4 {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO: przerobic przyklad 3, dodac relacje zwrotne
            em.find(Klient.class, 5L)
                .getOceny()
                    .forEach(ocena ->
                            System.out.println(
                                    ocena.getIdOceny()
                                            .getOcenianaKsiazka()
                                            .getTytul()));




           em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
