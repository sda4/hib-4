package sda.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example3 {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO: przerobic przyklad 2, zeby ocena byla zapisywana w tabeli ocena_ksiazki zamiast ocena

           em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
