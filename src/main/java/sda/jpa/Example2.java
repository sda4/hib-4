package sda.jpa;

import sda.jpa.model.AutorKsiazki;
import sda.jpa.model.Klient;
import sda.jpa.model.Ksiazka;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example2 {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();

            // TODO: przerobic przyklad 1, dodajac klase AutorKsiazki do klasy Ksiazka
            Ksiazka nowaKsiazka = Ksiazka.builder()
                    .rodzajKsiazki("Bajki")
                    .wiekMinimalnyCzytelnika(1)
                    .tytul("Czerwony kapturek")
                    .autor(AutorKsiazki.builder()
                            .imieAutora("Bialy")
                            .nazwiskoAutora("Wilk")
                            .build())
                    .build();

            em.persist(nowaKsiazka);
            em.getTransaction().commit();
            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
