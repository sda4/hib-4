package sda.jpa;

import sda.jpa.model.Recenzja;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Example5 {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO: zmapowac recenzje ksiazki

            List<Recenzja> recenzje = em.createQuery("select r from Recenzja r")
                    .getResultList();

          recenzje.forEach(rezencja -> System.out.println(rezencja.getTrescRecenzji()));


            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
