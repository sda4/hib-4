package sda.jpa;

import sda.jpa.model.Klient;
import sda.jpa.model.Ksiazka;
import sda.jpa.model.OcenaKey;
import sda.jpa.model.OcenaKsiazki;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Example1 {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("sda.jpa.1");

        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();

        // TODO: przerobic, zeby klient i ksiazka byly odczytane po ID, zamiast tworzone

        Klient klient = em.find(Klient.class, 6L);
        Ksiazka ksiazka = em.find(Ksiazka.class, 1L);

        System.out.println(ksiazka.getAutor().getImieAutora());

        OcenaKsiazki ocena = new OcenaKsiazki();
        ocena.setOcena(10);

        OcenaKey idOceny = new OcenaKey();
        idOceny.setOceniajacy(klient);
        idOceny.setOcenianaKsiazka(ksiazka);
        ocena.setIdOceny(idOceny);

        em.persist(ocena);

        transaction.commit();
        em.close();
        emf.close();
    }
}
