package sda.jpa.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name="klient")
public class Klient {

    @Id
    @Column(name = "klient_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long klientId;

    private String imie;

    private String nazwisko;

    @Enumerated(value = EnumType.STRING)
    private Plec plec;

    @OneToMany(mappedBy = "idOceny.oceniajacy")
    private List<OcenaKsiazki> oceny;

    @Transient
    private Long licznikKsiazek;
}
